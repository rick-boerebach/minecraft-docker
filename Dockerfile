FROM openjdk

ARG URL
ARG HASH

ADD $URL server.jar
RUN echo $HASH server.jar | sha1sum -c

RUN mkdir /data
VOLUME /data
WORKDIR /data

EXPOSE 25565/tcp

ENTRYPOINT ["java",  "-jar", "/server.jar"]

CMD ["nogui"]
