IMAGE ?= minecraft
$(IMAGE)\:%:
	docker pull "$@" &> /dev/null \
	|| docker inspect "$@" &> /dev/null \
	|| curl "https://launchermeta.mojang.com/mc/game/version_manifest.json" \
	| jq -r '.versions[] | select(.id=="$*") | .url' \
	| xargs curl \
	| jq -r '.downloads.server.url + " " + .downloads.server.sha1' \
	| (read -r "URL" "HASH"; export URL HASH; docker build --build-arg URL --build-arg HASH . -t $@)

LATEST = $(shell curl "https://launchermeta.mojang.com/mc/game/version_manifest.json" | jq -r '.latest.release')
$(IMAGE)\:latest: $(IMAGE)\:$(LATEST)
	docker tag $^ $@
